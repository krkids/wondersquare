﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PenballData{
    public class Item
    {
        public int itemId;
        public int itemCount;

        public Item (){

        }
        public Item (int id, int count) {
            this.itemId = id;
            this.itemCount = count;
        }
    }
}


