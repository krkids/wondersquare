﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PenballData{
    public class User {
        //UserId : 회원가입시 발행되는 난수
        public string userId;
        public string userEmail;
        public string userPassword;
        //NickName : 유저가 입력하는 값
        public string nickName;
        
        //Exp : 경험치
        public int exp;
        //UserLevel : 스테이지 패스를 통해 얻는 레벨
        public int userLevel;
        //유저가 gold로 업그레이드 3종 
        public int power;
        public int skill;
        public int earning;
        //멀티에서 체력
        public int health;
        //StageId : 현재 스테이지 
        public int stageId;
        //PenId : 선택된 펜 id
        public int penId;

        public int gold;
        public int highRank;
        public int rankingPoint;
        //Win : 승수
        public int win;
        public List<Item> items;

        //초기화 .기본 no arg
        public User (){
            
        }
        //초기화 .id 익명 생성때 사용
        public User (string id){
            userId = id;
        }
    }
}

