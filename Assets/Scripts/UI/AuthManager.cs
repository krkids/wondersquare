﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using PenballData;

public class AuthManager : MonoBehaviour
{
    AuthUI authUI;
    FirebaseAuth auth;
    FirebaseUser user;
    void Start()
    {
        authUI = GetComponent<AuthUI>();
        auth = FirebaseAuth.DefaultInstance;
        InitializeFirebase();
    }

    void InitializeFirebase()
    {
        //리스너 등록
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null); //초기값
    }
    //로그인 상태가 변경되었을 때 동작하는 리스너.
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                //signin 되었을때 상태
                enterLoggedin();
                
            }
        }
    }
    //로그인 성공시에 화면 전환
    void enterLoggedin(){
        
        Debug.Log("Signed in " + auth.CurrentUser.UserId);
        authUI.ShowLoginStatusPanel();
        authUI.loggedinUserEmail.text = user.Email;
        initDataBase();
    }
    /*
    데이타 베이스 관련
     */
    public void initDataBase(){
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://squareui.firebaseio.com/");
        FirebaseApp.DefaultInstance.SetEditorP12FileName("squareui-fa7f30221834.p12");
        FirebaseApp.DefaultInstance.SetEditorServiceAccountEmail("dev-yong@squareui.iam.gserviceaccount.com");
        FirebaseApp.DefaultInstance.SetEditorP12Password("notasecret");
    } 

    public void OnSaveDBButton(){
        string userId = auth.CurrentUser.UserId;
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string key = authUI.dbKey.text;
        string value = authUI.dbValue.text;
        Debug.Log("username="+userId);
        Debug.Log("set="+key+":"+value);
        reference.Child("users").Child(userId).Child(key).SetValueAsync(value).ContinueWith(
            task => {
                authUI.loggedinDataText.text = "username="+userId+"\n";
                authUI.loggedinDataText.text += "set="+key+":"+value+"\n";
                authUI.loggedinDataText.text += string.Format("OnClickSave::IsCompleted:{0} IsCanceled:{1} IsFaulted:{2}", task.IsCompleted, task.IsCanceled, task.IsFaulted);
                
            }
        );

    }
    public void OnLoadDBButton(){
        string userId = auth.CurrentUser.UserId;
        string key = authUI.dbKey.text;
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        reference.Child("users").Child(userId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted) {
            // Handle the error...
                authUI.loggedinDataText.text = "fail Load!!!";
            }
            else if (task.IsCompleted) {
                DataSnapshot snapshot = task.Result;
                authUI.dbValue.text = snapshot.Child(key).Value.ToString();
                Debug.Log(snapshot.Child(key).Value);
            }
      });
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void OnLoginButtonClick()
    {
        Debug.LogFormat(authUI.loginEmail.text, authUI.loginPassword.text);
        TryLoginWithFirbaseAuth(authUI.loginEmail.text, authUI.loginPassword.text);
    }
    public void OnCreateAccountButton()
    {
        TrySignupWithFirebaseAuth(authUI.signupEmail.text, authUI.signupPassword.text, authUI.signupConfirmPassword.text);
    }
    public void OnSignupButtonClick()
    {
        authUI.ShowSignupPanel();
    }
    public void OnSignoutButtonClick()
    {

        auth.SignOut();
        authUI.ShowLoginPanel();
    }
    private void TryLoginWithFirbaseAuth(string email, string password)
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }
            //로그인 성공
            FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId, newUser.Email);
        });
    }
    private void TrySignupWithFirebaseAuth(string email, string password, string confirmPassword)
    {
        if (password != confirmPassword)
        {
            Debug.Log("password matching error");
            return;
        }
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
        authUI.ShowLoginPanel();
    }
}
