﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//로그인/가입 페이지 UI 인터페이스 

public class AuthUI : MonoBehaviour
{
    public GameObject loginPanel;
    public GameObject signupPanel;
    public GameObject loginStatusPanel;

    public InputField loginEmail;
    public InputField loginPassword;
    
    public InputField signupEmail;
    public InputField signupPassword;
    public InputField signupConfirmPassword;

    public Text loggedinUserEmail;
    public Text loggedinDataText;

    public InputField dbKey;
    public InputField dbValue;

    // UI 캔버스의 시작
    void Start()
    {
        SetPasswordContents();
        ShowLoginPanel(); //초기화면 로그인 패널로.
        //ShowLoggedinPanel();
        loginEmail.text = "lazer24@naver.com";
        loginPassword.text = "tlrl1024";
    }
    //입력 Password를 ****로 표시
    private void SetPasswordContents(){
        loginPassword.contentType = InputField.ContentType.Password;
        signupPassword.contentType = InputField.ContentType.Password;
        signupConfirmPassword.contentType = InputField.ContentType.Password;
    }
    //Login Panel 로 전환
    public void ShowLoginPanel(){
        showPanel(loginPanel);
    }
    //Signup Panel 로 전환
    public void ShowSignupPanel(){
        showPanel(signupPanel);
    }
    //ShowLoggedin(로그인상태) Panel 로 전환
    public void ShowLoginStatusPanel(){
        
        showPanel(loginStatusPanel);
        Debug.Log("show logged");
        Debug.Log("loggedinPanel:"+loginStatusPanel.activeInHierarchy);
    }
    //패널간 전환 코드
    private void showPanel(GameObject panel){
        loginPanel.SetActive(false);
        signupPanel.SetActive(false);
        loginStatusPanel.SetActive(false);

        Debug.Log(" setsctivated");
        panel.SetActive(true);
    }
}
