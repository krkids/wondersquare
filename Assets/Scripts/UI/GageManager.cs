﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GageManager : MonoBehaviour
{
    //게이지 오브젝트
    public GameObject ScoreGage; 
    //게이지 텍스트
    public Text ScoreTxt;
    //score 값
    public float Score;
    //min-max min의 기본값 
    public int ScoreMin = 0;
    public int ScoreMax;

    private float ScoreRatio;
    private Vector2 ScoreVec;
    private RectTransform ScoreRT;
    void Awake(){
        if(ScoreGage != null){
            ScoreRT = ScoreGage.GetComponent<RectTransform>();
            ScoreVec = new Vector2(ScoreRT.rect.width, ScoreRT.rect.height);
        }
    }
    void Start(){
       ScoreRT.sizeDelta = new Vector2(ScoreVec.x*getRatio(ScoreMin, ScoreMax, Score), ScoreVec.y);
       ScoreTxt.text = Score.ToString("0");
    }
    void Update(){
       ScoreRT.sizeDelta = new Vector2(ScoreVec.x*getRatio(ScoreMin, ScoreMax, Score), ScoreVec.y);
       ScoreTxt.text = Score.ToString("0");
    }
    
    //min max 값과 현재 값으로 ratio 를 가져옴
    float getRatio(int min, int max, float value){
        return value/(max-min);
    }
}
